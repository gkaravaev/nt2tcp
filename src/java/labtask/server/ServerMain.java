package server;

import java.io.IOException;

public class ServerMain {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Not enough args!");
        }
        Server server = new Server(Integer.parseInt(args[0]));
        server.listen();
    }
}
